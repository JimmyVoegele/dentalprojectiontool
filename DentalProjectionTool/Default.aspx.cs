﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Telerik.Web.UI;

namespace DentalProjectionTool
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RadComboBoxItem RCB = new RadComboBoxItem("Blue", "1");
                cmbTeamColor.Items.Add(RCB);
                RCB = new Telerik.Web.UI.RadComboBoxItem("Orange", "2");
                cmbTeamColor.Items.Add(RCB);
                RCB = new Telerik.Web.UI.RadComboBoxItem("Purple", "3");
                cmbTeamColor.Items.Add(RCB);
                RCB = new Telerik.Web.UI.RadComboBoxItem("Red", "4");
                cmbTeamColor.Items.Add(RCB);
                RadGrid2.Rebind();
                lblStudentCount.Text = RadGrid2.Items.Count.ToString();
            }
        }

        protected void cmbTeamColor_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            SqlDataSource4.SelectCommand = "Select school_name, Team, student_first_name, student_last_name, student_phone, Grade, ReceivedTreament from [dbo].[vwDentalProjectSummary]  where Team = '" + cmbTeamColor.Text + "'";
            RadGrid2.Rebind();
            lblStudentCount.Text = RadGrid2.Items.Count.ToString();
        }
    }
}