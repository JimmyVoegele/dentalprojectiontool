﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DentalProjectionTool._Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2 style="color:#0091ba">PROJECTION TOOL OVERVIEW</h2>
    <asp:image runat="server"  ImageUrl="~/Images/OverviewImage.png"></asp:image>
    <br />
    <br />
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Office2007">
        <Tabs>
            <telerik:radtab Text="All Dental" Width="200px"></telerik:RadTab>
            <telerik:radtab Text="All Dental (DMT Only)" Width="200px" Selected="True"></telerik:RadTab>
            <telerik:radtab Text="Team" Width="200px"></telerik:RadTab>
            <telerik:radtab Text="Admin" Width="200px"></telerik:RadTab>
            <telerik:radtab Text="What IF..." Width="200px"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0">
        <telerik:radpageview runat="server" ID="RadPageView1">
            <br />
            <telerik:RadLabel runat="server" Text="Summer Planning Assumptions" Font-Bold="true"></telerik:RadLabel>
            <br />
            <telerik:RadGrid ID="RadGridOverview"  DataSourceID="SqlDataSource1" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="None">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                    <ColumnGroups>
                        <telerik:GridColumnGroup HeaderText="Restorative Kids" Name="RestorativeKids" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="Restorative Rate" Name="RestorativeRate" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="Work Days" Name="WorkDays" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="PACE" Name="Pace" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="End Date" Name="EndDate" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                    </ColumnGroups>
                    <Columns>
                        <telerik:GridBoundColumn UniqueName="Team" DataField="Team" HeaderText="Team">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorativeProjected" DataField="RestorativeProjected" ColumnGroupName="RestorativeKids" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorativeSoFar" DataField="RestorativeSoFar" ColumnGroupName="RestorativeKids" HeaderText="So Far">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorRateProjected" DataField="RestorRateProjected" ColumnGroupName="RestorativeRate" DataFormatString="{0:P}" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorRateActual" DataField="RestorRateActual" ColumnGroupName="RestorativeRate"  DataFormatString="{0:P}" HeaderText="So Far">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="DaysProjected" DataField="DaysProjected" ColumnGroupName="WorkDays" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="DaysActual" DataField="DaysActual" ColumnGroupName="WorkDays" HeaderText="So Far">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="ProjectedPace" DataField="ProjectedPace" ColumnGroupName="Pace" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="ActualPace" DataField="ActualPace" ColumnGroupName="Pace" HeaderText="Actual">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="EndDateProjected" DataField="EndDateProjected" ColumnGroupName="EndDate" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="EndDateActual" DataField="EndDateActual" ColumnGroupName="EndDate" HeaderText="Actual">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <br />
            <br />
            <telerik:RadLabel runat="server" Text="Current Goal (Reduced)" Font-Bold="true"></telerik:RadLabel>
            <br />
            <telerik:RadGrid ID="RadGridCurrentGoals"  DataSourceID="SqlDataSource2" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007" ShowFooter="true">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="None">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridBoundColumn UniqueName="Team" DataField="Team" HeaderText="Team" FooterStyle-BackColor="White" FooterStyle-BorderColor="Black" FooterText="Totals">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="Roster" DataField="Roster" HeaderText="Roster"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestWhite" DataField="RestWhite" HeaderText="Rest White"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestAccept" DataField="RestAccept" HeaderText="Rest Accpt"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestTx" DataField="RestTx" HeaderText="Rest Tx" Aggregate="Sum" FooterAggregateFormatString="{0:N0}"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="TotalPts" DataField="TotalPts" HeaderText="Total Pts" Aggregate="Sum" FooterAggregateFormatString="{0:N0}"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
            <br />
            <br />
            <telerik:RadLabel runat="server" Text="Last Yr Actuals" Font-Bold="true"></telerik:RadLabel>
            <br />
            <telerik:RadGrid ID="RadGrid1"  DataSourceID="SqlDataSource3" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007" ShowFooter="true">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="False" CommandItemDisplay="None">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                    <Columns>
                        <telerik:GridBoundColumn UniqueName="Team" DataField="Team" HeaderText="Team" FooterStyle-BackColor="White" FooterStyle-BorderColor="Black" FooterText="Totals">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="Roster" DataField="Roster" HeaderText="Roster"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestWhite" DataField="RestWhite" HeaderText="Rest White"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestAccept" DataField="RestAccept" HeaderText="Rest Accpt"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestTx" DataField="RestTx" HeaderText="Rest Tx" Aggregate="Sum" FooterAggregateFormatString="{0:N0}"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="TotalPts" DataField="TotalPts" HeaderText="Total Pts" Aggregate="Sum" FooterAggregateFormatString="{0:N0}"  FooterStyle-BackColor="White" FooterStyle-BorderColor="Black">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
         </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView2">
            <br />
            <telerik:RadLabel runat="server" Text="Summer Planning Assumptions (DMT Only)" Font-Bold="true"></telerik:RadLabel>
            <br />
            <telerik:RadGrid ID="RadGrid3"  DataSourceID="SqlDataSource1" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="false" CommandItemDisplay="None">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                    <ColumnGroups>
                        <telerik:GridColumnGroup HeaderText="Restorative Kids" Name="RestorativeKids" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="Restorative Rate" Name="RestorativeRate" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="Work Days" Name="WorkDays" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="PACE" Name="Pace" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                        <telerik:GridColumnGroup HeaderText="End Date" Name="EndDate" HeaderStyle-HorizontalAlign="Center">
                        </telerik:GridColumnGroup>
                    </ColumnGroups>
                    <Columns>
                        <telerik:GridBoundColumn UniqueName="Team" DataField="Team" HeaderText="Team">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorativeProjected" DataField="RestorativeProjected" ColumnGroupName="RestorativeKids" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorativeSoFar" DataField="RestorativeSoFar" ColumnGroupName="RestorativeKids" HeaderText="So Far">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorRateProjected" DataField="RestorRateProjected" ColumnGroupName="RestorativeRate" DataFormatString="{0:P}" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="RestorRateActual" DataField="RestorRateActual" ColumnGroupName="RestorativeRate"  DataFormatString="{0:P}" HeaderText="So Far">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="DaysProjected" DataField="DaysProjected" ColumnGroupName="WorkDays" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="DaysActual" DataField="DaysActual" ColumnGroupName="WorkDays" HeaderText="So Far">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="ProjectedPace" DataField="ProjectedPace" ColumnGroupName="Pace" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="ActualPace" DataField="ActualPace" ColumnGroupName="Pace" HeaderText="Actual">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="EndDateProjected" DataField="EndDateProjected" ColumnGroupName="EndDate" HeaderText="Projected">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn UniqueName="EndDateActual" DataField="EndDateActual" ColumnGroupName="EndDate" HeaderText="Actual">
                        </telerik:GridBoundColumn>
                    </Columns>
                </MasterTableView>
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView3">
            <br />
            <br />
            <telerik:RadLabel ID="RadLabel6" Text="Team :" runat="server" Skin="Office2007" Font-Bold="true" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadComboBox ID="cmbTeamColor" AutoPostBack ="true"  Width="120px" runat="server" OnSelectedIndexChanged="cmbTeamColor_SelectedIndexChanged"></telerik:RadComboBox >
            <br />
            <br />
            <telerik:RadLabel ID="RadLabel1" Text="Student Count:" runat="server" style='padding-left:50px'></telerik:RadLabel>
            <telerik:RadLabel ID="lblStudentCount" Text="100" runat="server"></telerik:RadLabel>
            <br />
            <br />
            <telerik:RadGrid ID="RadGrid2"  DataSourceID="SqlDataSource4" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="True" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                </MasterTableView>
            </telerik:RadGrid>
         </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView4">
        </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView5">
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select Team, RestorativeProjected, RestorativeSoFar, RestorRateProjected, RestorRateActual, DaysProjected, DaysActual, ActualPace, ProjectedPace, EndDateProjected, EndDateActual from vwDentalProjectionAssumptions">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select Team, Roster, RestWhite, RestAccept, RestTx, TotalPts from vwDentalTeamData where Year = 2018">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select Team, Roster, RestWhite, RestAccept, RestTx, TotalPts from vwDentalTeamData where Year = 2017">
    </asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select school_name, Team, student_first_name, student_last_name, student_phone, Grade, ReceivedTreament from [dbo].[vwDentalProjectSummary]  where Team = 'Blue'">
    </asp:SqlDataSource>
</asp:Content>
