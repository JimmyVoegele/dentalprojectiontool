﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DentalProjectionTool._Default" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h2 style="color:#0091ba">Dental Projection Tool</h2>
    <br />
    <br />
    <telerik:RadTabStrip RenderMode="Lightweight" runat="server" ID="RadTabStrip1"  MultiPageID="RadMultiPage1" SelectedIndex="0" Skin="Office2007">
        <Tabs>
            <telerik:radtab Text="Overview" Width="200px" Selected="True"></telerik:RadTab>
            <telerik:radtab Text="Team Summary" Width="200px"></telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage runat="server" ID="RadMultiPage1"  SelectedIndex="0">
        <telerik:radpageview runat="server" ID="RadPageView1">
            <telerik:RadGrid ID="RadGrid2"  DataSourceID="SqlDataSource1" runat="server" ClientSettings-Resizing-AllowColumnResize="true" Skin="Office2007">
                <GroupingSettings CollapseAllTooltip="Collapse all groups" />
                <ClientSettings>
                    <Resizing AllowColumnResize="True" />
                </ClientSettings>
                <MasterTableView AutoGenerateColumns="True" CommandItemDisplay="Top">
                    <CommandItemSettings ShowExportToExcelButton="true" ShowRefreshButton="false" ShowCancelChangesButton="false" ShowSaveChangesButton="false" ShowAddNewRecordButton="false" />
                </MasterTableView>
            </telerik:RadGrid>
         </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView2">
        </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView3">
         </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView4">
        </telerik:RadPageView>
        <telerik:radpageview runat="server" ID="RadPageView5">
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="Select school_name, Team, student_first_name, student_last_name, student_phone, Grade, ReceivedTreament, CallTime from vwDentalProjectSummary order by CallTime desc, ReceivedTreament desc">
    </asp:SqlDataSource>
</asp:Content>
